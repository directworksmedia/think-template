from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse


def anonymous_required(view_func):
    """
    Only allow anonymous (non-authenticated) users to access a specific view.
    """

    def wrap(request, *args, **kwargs):
        if not request.user.is_anonymous:
            return redirect(reverse("authentication:dashboard"))
        return view_func(request, *args, **kwargs)

    return wrap


def user_required(view_func):
    """
    Only allow users marked as users to access a specific view.
    """

    def wrap(request, *args, **kwargs):
        if not request.user.is_user:
            messages.error(request, "You need to be a user to perform this action.")
            return redirect(request.META.get("HTTP_REFERER"))
        return view_func(request, *args, **kwargs)

    return wrap


def admin_required(view_func):
    """
    Only allow users marked as admins to access a specific view.
    """

    def wrap(request, *args, **kwargs):
        if not request.user.is_admin:
            messages.error(request, "You need to be an admin to perform this action.")
            return redirect(request.META.get("HTTP_REFERER"))
        return view_func(request, *args, **kwargs)

    return wrap
