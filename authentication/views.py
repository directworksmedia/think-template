from django.contrib.auth.decorators import login_required
from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect
from django.urls import reverse

from authentication.models import User


@login_required
def dashboard(request: HttpRequest) -> HttpResponse:
    """
    Redirects the user to their appropriate dashboard.
    """
    user: User = request.user
    if user.is_admin:
        return redirect(reverse("admins:dashboard"))

    if user.is_user:
        return redirect(reverse("users:dashboard"))
