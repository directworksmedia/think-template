from django.contrib import messages
from django.contrib.auth import login as base_login, logout as base_logout, authenticate
from django.contrib.auth.decorators import login_required
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse

from authentication.decorators import anonymous_required, user_required
from authentication.forms import LoginForm


@anonymous_required
def login(request: HttpRequest) -> HttpResponse:
    """
    Authenticates and logs in a user.
    """
    if request.method == "POST":
        form = LoginForm(data=request.POST, form_action="users:login")
        if form.is_valid():
            user = authenticate(
                request,
                email=form.cleaned_data["username"],
                password=form.cleaned_data["password"],
            )

            if not user.is_user:
                messages.error(
                    request, "You need to be a user to perform this action."
                )
            else:
                base_login(request, user)
                messages.success(request, "You have been logged in.")
                return redirect(reverse("users:dashboard"))
    else:
        form = LoginForm(form_action="users:login")

    ctx = {
        "form": form,
    }

    return render(request, "users/login.html", ctx)


@login_required
def logout(request: HttpRequest) -> HttpResponse:
    """
    Logs a user out of the system.
    """
    base_logout(request)
    messages.error(request, "You have been logged out.")
    return redirect(reverse("users:login"))


@login_required
@user_required
def dashboard(request: HttpRequest) -> HttpResponse:
    """
    Main user dashboard
    """
    return render(request, "users/dashboard.html")
