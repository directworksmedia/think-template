from django.urls import path

from admins import views

app_name = "admins"

urlpatterns = [
    path("login/", views.login, name="login"),
    path("logout/", views.logout, name="logout"),
    path("dashboard/", views.dashboard, name="dashboard"),
    path("dashboard/settings/", views.settings, name="settings"),
    path(
        "dashboard/settings/change_password/",
        views.settings_change_password,
        name="settings_change_password",
    ),
    path("register/", views.register, name="register"),
    path("forgot_password/", views.forgot_password, name="forgot_password"),
    path(
        "forgot_password_code/<str:email>/",
        views.forgot_password_code,
        name="forgot_password_code",
    ),
    path(
        "resend_password_code/<str:email>/",
        views.resend_password_code,
        name="resend_password_code",
    ),
    path(
        "change_password/<str:email>/<str:code>/",
        views.change_password,
        name="change_password",
    ),
]
