from crispy_forms.bootstrap import FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field, HTML, Div
from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.utils.safestring import mark_safe

from admins.models import AdminProfile
from authentication.models import User, PasswordResetCode


class RegistrationForm(forms.Form):
    first_name = forms.CharField(label="First Name")
    last_name = forms.CharField(label="Last Name")
    role = forms.ChoiceField(choices=AdminProfile.Roles.choices, label="Your Role")
    email = forms.EmailField(label="Email Address")
    phone_number = forms.CharField(max_length=25, label="Phone Number")
    password1 = forms.CharField(
        widget=forms.PasswordInput, label="Password", validators=[validate_password]
    )
    password2 = forms.CharField(widget=forms.PasswordInput, label="Confirm Password")
    accept_toc = forms.BooleanField(
        required=True,
        label=mark_safe(
            "By checking this checkbox you fully understand & agree our "
            "<a href='#' class='txt-color-gold'>Privacy Policy</a>"
            " and "
            "<a href='#' class='txt-color-gold'>Terms and Conditions</a>."
        ),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_id = "id-registrationForm"
        self.helper.form_class = "w-100 m-0 p-0"
        self.helper.form_method = "post"
        self.helper.form_action = "admins:register"
        self.helper.label_class = "form-label"

        submit_button = Submit(
            "submit",
            "Register",
            css_id="get_started",
        )
        submit_button.field_classes = (
            "btn btn-solid-gold w-100 d-table text-center mt-5"
        )

        self.helper.layout = Layout(
            Field(
                "first_name",
                placeholder="Enter First Name",
            ),
            Field(
                "last_name",
                placeholder="Enter Last Name",
            ),
            Field(
                "email",
                placeholder="Enter Email Address",
            ),
            "role",
            Field(
                "phone_number",
                placeholder="000 000 000",
                id="phone_num",
            ),
            Field(
                "password1",
                placeholder="Enter Password",
            ),
            Field(
                "password2",
                placeholder="Re-enter Password",
            ),
            Field(
                "accept_toc",
                css_class="txt-color-gray",
            ),
            FormActions(
                submit_button,
            ),
        )

    def clean_email(self):
        email = self.cleaned_data.get("email")
        if User.objects.filter(email=email):
            raise ValidationError("This email already exists.")

        return email

    def clean(self):
        cleaned_data = super().clean()
        if not self.errors.get("password1"):
            password1 = cleaned_data.get("password1")
            password2 = cleaned_data.get("password2")

            if password1 != password2:
                raise ValidationError({"password1": ["Passwords do not match."]})


class ForgotPasswordForm(forms.Form):
    email = forms.EmailField(label="Email Address")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_id = "id-forgotPasswordForm"
        self.helper.form_class = "w-100 m-0 p-0"
        self.helper.form_method = "post"
        self.helper.form_action = "admins:forgot_password"
        self.helper.label_class = "form-label"

        submit_button = Submit(
            "submit",
            "Submit",
            css_id="get_started",
        )
        submit_button.field_classes = (
            "btn btn-solid-gold w-100 d-table text-center mt-5"
        )

        self.helper.layout = Layout(
            Field(
                "email",
                placeholder="Enter Email Address",
            ),
            FormActions(
                submit_button,
            ),
        )


class ConfirmForgotPasswordCodeForm(forms.Form):
    char1 = forms.CharField(max_length=1)
    char2 = forms.CharField(max_length=1)
    char3 = forms.CharField(max_length=1)
    char4 = forms.CharField(max_length=1)

    email: str

    def __init__(self, *args, **kwargs):
        self.email = kwargs.pop("email")
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_id = "id-forgotPasswordCodeForm"
        self.helper.form_class = "w-100 m-0 p-0 mb-5"
        self.helper.form_method = "post"
        self.helper.form_action = reverse(
            "admins:forgot_password_code", args=[self.email]
        )
        self.helper.label_class = "form-label"

        submit_button = Submit(
            "submit",
            "Submit",
            css_id="get_started",
        )
        submit_button.field_classes = (
            "btn btn-solid-gold w-100 d-table text-center mt-5"
        )

        resend_button = HTML(
            f'<a href="{reverse("admins:resend_password_code", args=[self.email])}" class="btn btn-outline-gold mt-3 d-table w-100">Resend Code</a>'
        )

        self.helper.layout = Layout(
            Div(
                HTML(
                    '<input type="text" class="char_code_verify" id="char1" name="char1" data-next="char2" maxlength="1">'
                ),
                HTML(
                    '<input type="text" class="char_code_verify" id="char2" name="char2" data-next="char3" maxlength="1">'
                ),
                HTML(
                    '<input type="text" class="char_code_verify" id="char3" name="char3" data-next="char4" maxlength="1">'
                ),
                HTML(
                    '<input type="text" class="char_code_verify" id="char4" name="char4" data-next="char5" maxlength="1">'
                ),
                css_class="form-group d-table digit-group",
            ),
            FormActions(
                submit_button,
                resend_button,
                css_class="form-group",
            ),
        )

    def clean(self):
        cleaned_data = super().clean()
        code = f"{cleaned_data['char1']}{cleaned_data['char2']}{cleaned_data['char3']}{cleaned_data['char4']}"
        try:
            password_reset_code = PasswordResetCode.objects.get(
                user__email=self.email,
                code=code,
            )

            if password_reset_code.has_expired:
                raise ValidationError("Code has already expired.")
        except PasswordResetCode.DoesNotExist:
            raise ValidationError("Incorrect code.")


class ChangePasswordForm(forms.Form):
    password = forms.CharField(
        widget=forms.PasswordInput, label="New Password", validators=[validate_password]
    )

    email: str
    code: str

    def __init__(self, *args, **kwargs):
        self.email = kwargs.pop("email")
        self.code = kwargs.pop("code")
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_id = "id-changePasswordForm"
        self.helper.form_class = "w-100 m-0 p-0"
        self.helper.form_method = "post"
        self.helper.form_action = reverse(
            "admins:change_password", args=[self.email, self.code]
        )
        self.helper.label_class = "form-label"

        submit_button = Submit(
            "submit",
            "Submit",
            css_id="get_started",
        )
        submit_button.field_classes = "btn btn-solid-gold w-100 d-table text-center"

        self.helper.layout = Layout(
            Field("password", placeholder="Enter Password"),
            FormActions(
                submit_button,
            ),
        )

    def clean(self):
        super().clean()
        try:
            password_reset_code = PasswordResetCode.objects.get(
                user__email=self.email,
                code=self.code,
            )

            if password_reset_code.has_expired:
                raise ValidationError("Code has already expired.")

        except PasswordResetCode.DoesNotExist:
            raise ValidationError("Incorrect code.")


class SettingsForm(forms.Form):
    admin: User

    first_name = forms.CharField(label="First Name")
    last_name = forms.CharField(label="Last Name")
    role = forms.ChoiceField(choices=AdminProfile.Roles.choices, label="Your Role")
    email = forms.EmailField(label="Email Address")
    phone_number = forms.CharField(max_length=25, label="Phone Number")
    receive_email_notifications = forms.BooleanField(
        required=False, label="Receive Email Notifications"
    )

    def __init__(self, *args, **kwargs):
        self.admin = kwargs.pop("admin")
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_id = "id-settingsForm"
        self.helper.form_class = "row mt-5"
        self.helper.form_method = "post"
        self.helper.form_action = reverse("admins:settings")
        self.helper.label_class = "form-label"

        submit_button = Submit(
            "submit",
            "Save",
        )
        submit_button.field_classes = "btn btn-solid-gold w-100"

        self.helper.layout = Layout(
            Div(
                Div(
                    HTML(
                        '<span class="fs-11 fw-700 txt-color-gray text-uppercase">Personal Details</span>'
                    ),
                    Div(
                        Div(
                            Field("first_name"),
                            css_class="col-12 col-md-6",
                        ),
                        Div(
                            Field("last_name"),
                            css_class="col-12 col-md-6",
                        ),
                        Div(
                            Field("role"),
                            css_class="col-12 col-md-6",
                        ),
                        Div(
                            Field(
                                "phone_number",
                                id="phone_num",
                            ),
                            css_class="col-12 col-md-6",
                        ),
                        Div(
                            Field("email"),
                            css_class="col-12 col-md-6",
                        ),
                        css_class="form-row pl-3 pt-3 border-left-gray-80-1 mx-0",
                    ),
                    css_class="w-100 mb-4",
                ),
                css_class="col-12",
            ),
            Div(
                HTML(
                    '<span class="fs-11 fw-700 txt-color-gray text-uppercase">Notifications</span>'
                ),
                Div(
                    Div(
                        Div(
                            Field("receive_email_notifications"),
                            css_class="col-12",
                        ),
                        css_class="form-row pl-3 pt-3 border-left-gray-80-1 mx-0",
                    ),
                    css_class="w-100 mb-4",
                ),
                css_class="col-12",
            ),
            FormActions(submit_button, css_class="col-md-4 mb-3"),
        )

    def clean_email(self):
        cleaned_email = self.cleaned_data["email"]
        if User.objects.filter(email=cleaned_email).exclude(email=self.admin.email):
            raise ValidationError("Email is already taken.")

        return cleaned_email


class SettingsChangePasswordForm(forms.Form):
    admin: User

    current_password = forms.CharField(
        widget=forms.PasswordInput, label="Current Password"
    )
    password1 = forms.CharField(
        widget=forms.PasswordInput,
        label="Set New Password",
        validators=[validate_password],
    )
    password2 = forms.CharField(widget=forms.PasswordInput, label="Confirm Password")

    def __init__(self, *args, **kwargs):
        self.admin = kwargs.pop("admin")
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_id = "id-settingsChangePasswordForm"
        self.helper.form_class = "w-100 m-0 p-0"
        self.helper.form_method = "post"
        self.helper.form_action = reverse("admins:settings_change_password")
        self.helper.label_class = "form-label"

        submit_button = Submit(
            "submit",
            "Save",
            css_id="get_started",
        )
        submit_button.field_classes = "btn btn-solid-gold w-100 d-table text-center"

        self.helper.layout = Layout(
            Field(
                "current_password",
                placeholder="Enter Current Password",
            ),
            Field(
                "password1",
                placeholder="Enter New Password",
            ),
            Field(
                "password2",
                placeholder="Confirm New Password",
            ),
            FormActions(
                submit_button,
            ),
        )

    def clean_current_password(self):
        cleaned_current_password = self.cleaned_data["current_password"]
        if not authenticate(email=self.admin.email, password=cleaned_current_password):
            raise ValidationError("Password is incorrect.")

        return cleaned_current_password

    def clean(self):
        cleaned_data = super().clean()
        if not self.errors.get("password1"):
            password1 = cleaned_data.get("password1")
            password2 = cleaned_data.get("password2")

            if password1 != password2:
                raise ValidationError({"password1": ["Passwords do not match."]})
