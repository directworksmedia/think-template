from django.contrib import admin

from admins.models import AdminProfile, Admin


class AdminProfileInline(admin.StackedInline):
    model = AdminProfile


class AdminAdmin(admin.ModelAdmin):
    inlines = (AdminProfileInline,)


admin.site.register(Admin, AdminAdmin)
