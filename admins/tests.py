from django.contrib.auth import authenticate
from django.core import mail
from django.test import TestCase

from admins.models import AdminProfile
from admins.services import (
    create_admin_account,
    send_password_reset_code,
    change_user_password,
    update_admin_settings,
)
from authentication.models import User, PasswordResetCode


class AdminServicesTest(TestCase):
    def test_can_create_new_admin_accounts(self):
        data = {
            "first_name": "John",
            "last_name": "Doe",
            "email": "jdoe@example.com",
            "role": AdminProfile.Roles.CAPTAIN,
            "phone_number": "+123456789",
            "password": "p@ssword!",
        }

        admin = create_admin_account(**data)

        self.assertEqual(admin.first_name, data["first_name"])
        self.assertEqual(admin.last_name, data["last_name"])
        self.assertEqual(admin.email, data["email"])

        # An AdminProfile instance will be created as well.
        self.assertEqual(admin.admin_profile.role, data["role"])
        self.assertEqual(admin.admin_profile.phone_number, data["phone_number"])

    def test_can_send_password_reset_code(self):
        admin = User.objects.create(email="admin@example.com")
        code_has_been_sent = send_password_reset_code(email=admin.email)

        code = PasswordResetCode.objects.get(user=admin)
        self.assertIn(str(code), mail.outbox[0].body)
        self.assertTrue(code_has_been_sent)

    def test_will_not_send_a_code_to_unused_emails(self):
        unused_email = "unused_email@example.com"
        code_has_been_sent = send_password_reset_code(email=unused_email)

        self.assertFalse(code_has_been_sent)
        self.assertFalse(PasswordResetCode.objects.filter(user__email=unused_email))

    def test_can_change_a_users_password(self):
        admin = User.objects.create(email="test@example.com")
        new_password = "p@ssword!"
        change_user_password(email=admin.email, password=new_password)

        self.assertTrue(authenticate(email=admin.email, password=new_password))

    def test_can_update_a_users_settings(self):
        admin = User.objects.create(
            email="test@example.com",
            first_name="John",
            last_name="Doe",
        )
        AdminProfile.objects.create(
            user=admin,
            phone_number="+123456789",
            role=AdminProfile.Roles.CAPTAIN,
            receive_email_notifications=True,
        )

        updated_data = {
            "email": "updated@example.com",
            "first_name": "Adam",
            "last_name": "Smith",
            "phone_number": "+987654321",
            "role": AdminProfile.Roles.PURSER,
            "receive_email_notifications": False,
        }

        update_admin_settings(
            admin=admin,
            **updated_data,
        )

        admin.refresh_from_db()
        admin.admin_profile.refresh_from_db()

        self.assertEquals(updated_data["email"], admin.email)
        self.assertEquals(updated_data["first_name"], admin.first_name)
        self.assertEquals(updated_data["last_name"], admin.last_name)
        self.assertEquals(
            updated_data["phone_number"], admin.admin_profile.phone_number
        )
        self.assertEquals(updated_data["role"], admin.admin_profile.role)
        self.assertEquals(
            updated_data["receive_email_notifications"],
            admin.admin_profile.receive_email_notifications,
        )
