from django.contrib import messages
from django.contrib.auth import login as base_login, logout as base_logout, authenticate
from django.contrib.auth.decorators import login_required
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse

from admins.forms import (
    RegistrationForm,
    ForgotPasswordForm,
    ConfirmForgotPasswordCodeForm,
    ChangePasswordForm,
    SettingsForm,
    SettingsChangePasswordForm,
)
from admins.services import (
    create_admin_account,
    send_password_reset_code,
    change_user_password,
    update_admin_settings,
)
from authentication.decorators import anonymous_required, admin_required
from authentication.forms import LoginForm
from authentication.models import User


@anonymous_required
def login(request: HttpRequest) -> HttpResponse:
    """
    Authenticates and logs in an admin.
    """
    if request.method == "POST":
        form = LoginForm(data=request.POST, form_action="admins:login")
        if form.is_valid():
            user = authenticate(
                request,
                email=form.cleaned_data["username"],
                password=form.cleaned_data["password"],
            )

            if not user.is_admin:
                messages.error(
                    request, "You need to be an admin to perform this action."
                )
                return redirect(request.META.get("HTTP_REFERER"))

            else:
                base_login(request, user)
                messages.success(request, "You have been logged in.")
                return redirect(reverse("admins:dashboard"))
    else:
        form = LoginForm(form_action="admins:login")

    ctx = {
        "form": form,
    }

    return render(request, "admins/login.html", ctx)


@login_required
def logout(request: HttpRequest) -> HttpResponse:
    """
    Logs an admin out of the system.
    """
    base_logout(request)
    messages.error(request, "You have been logged out.")
    return redirect(reverse("admins:login"))


@anonymous_required
def register(request: HttpRequest) -> HttpResponse:
    """
    Register a new admin account.
    """
    if request.method == "POST":
        form = RegistrationForm(data=request.POST)
        if form.is_valid():
            user = create_admin_account(
                first_name=form.cleaned_data["first_name"],
                last_name=form.cleaned_data["last_name"],
                email=form.cleaned_data["email"],
                role=form.cleaned_data["role"],
                phone_number=form.cleaned_data["phone_number"],
                password=form.cleaned_data["password1"],
            )
            base_login(request, user)
            messages.success(request, "Your account is now created!")
            return redirect(reverse("admins:dashboard"))
    else:
        form = RegistrationForm()

    ctx = {
        "form": form,
    }

    return render(request, "admins/registration.html", ctx)


@anonymous_required
def forgot_password(request: HttpRequest) -> HttpResponse:
    """
    Entry point for the forgot password flow.
    """
    if request.method == "POST":
        form = ForgotPasswordForm(data=request.POST)
        if form.is_valid():
            email = form.cleaned_data["email"]
            send_password_reset_code(email=email)
            return redirect(reverse("admins:forgot_password_code", args=[email]))
    else:
        form = ForgotPasswordForm()

    ctx = {
        "form": form,
    }

    return render(request, "admins/forgot_password.html", ctx)


@anonymous_required
def forgot_password_code(request: HttpRequest, email: str) -> HttpResponse:
    """
    Accepts the entered data from the forgot_password view for further processing.
    """
    if request.method == "POST":
        form = ConfirmForgotPasswordCodeForm(data=request.POST, email=email)
        if form.is_valid():
            code = f"{form.cleaned_data['char1']}{form.cleaned_data['char2']}{form.cleaned_data['char3']}{form.cleaned_data['char4']}"
            return redirect(reverse("admins:change_password", args=[email, code]))
    else:
        form = ConfirmForgotPasswordCodeForm(email=email)

    ctx = {
        "email": email,
        "form": form,
    }

    return render(request, "admins/forgot_password_code.html", ctx)


@anonymous_required
def resend_password_code(request: HttpRequest, email: str) -> HttpResponse:
    """
    Resends a new password reset code.
    """
    send_password_reset_code(email=email)
    messages.info(request, "Password reset code re-sent.")
    return redirect(reverse("admins:forgot_password_code", args=[email]))


@anonymous_required
def change_password(request: HttpRequest, email: str, code: str) -> HttpResponse:
    """
    Last step of the forgot password flow that actually accepts a new password from the user.
    """
    if request.method == "POST":
        form = ChangePasswordForm(data=request.POST, email=email, code=code)
        if form.is_valid():
            change_user_password(email=email, password=form.cleaned_data["password"])
            messages.success(request, "Password successfully changed")
            return redirect(reverse("admins:login"))
    else:
        form = ChangePasswordForm(email=email, code=code)

    ctx = {
        "form": form,
    }
    return render(request, "admins/change_password.html", ctx)


@login_required
@admin_required
def dashboard(request: HttpRequest) -> HttpResponse:
    """
    Main admin dashboard
    """
    return render(request, "admins/dashboard.html")


@login_required
@admin_required
def settings(request: HttpRequest) -> HttpResponse:
    """
    Settings page for the admin.
    """
    admin: User = request.user
    initial = {
        "first_name": admin.first_name,
        "last_name": admin.last_name,
        "email": admin.email,
        "role": admin.admin_profile.role,
        "phone_number": admin.admin_profile.phone_number,
        "receive_email_notifications": admin.admin_profile.receive_email_notifications,
    }
    if request.method == "POST":
        form = SettingsForm(
            data=request.POST,
            initial=initial,
            admin=admin,
        )
        if form.is_valid():
            update_admin_settings(
                admin=admin,
                **form.cleaned_data,
            )
            messages.success(request, "Your settings has been updated.")
            return redirect(reverse("admins:dashboard"))
    else:
        form = SettingsForm(initial=initial, admin=admin)

    ctx = {
        "form": form,
    }

    return render(request, "admins/settings.html", ctx)


@login_required
@admin_required
def settings_change_password(request: HttpRequest) -> HttpResponse:
    """
    Change password form inside the admin dashboard.
    """
    admin: User = request.user

    if request.method == "POST":
        form = SettingsChangePasswordForm(
            data=request.POST,
            admin=admin,
        )
        if form.is_valid():
            change_user_password(
                email=admin.email, password=form.cleaned_data["password1"]
            )
            base_logout(request)
            messages.success(request, "Your password has been updated.")
            return redirect(reverse("admins:login"))
    else:
        form = SettingsChangePasswordForm(admin=admin)

    ctx = {"form": form}

    return render(request, "admins/settings_change_password.html", ctx)
