from django.conf import settings
from django.db import models

from admins.managers import AdminManager
from authentication.models import User


class Admin(User):
    """
    Proxy model for administrators.
    """

    class Meta:
        proxy = True

    objects = AdminManager()


class AdminProfile(models.Model):
    """
    Contains the information for an admin account.
    """

    class Roles(models.TextChoices):
        LEAD  = ("LEAD", "Lead")
        STAFF  = ("STAFF", "Staff")

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="admin_profile"
    )
    phone_number = models.CharField(max_length=25)
    role = models.CharField(max_length=255, choices=Roles.choices)
    receive_email_notifications = models.BooleanField(default=True)

    def __str__(self):
        return str(self.user)
