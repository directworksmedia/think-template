from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string

from admins.models import AdminProfile
from authentication.models import User, PasswordResetCode


def create_admin_account(
    *,
    first_name: str,
    last_name: str,
    email: str,
    role: str,
    phone_number: str,
    password: str,
) -> User:
    """
    Creates an Administrator account given the details provided.
    """
    user = User.objects.create(
        first_name=first_name, last_name=last_name, email=email, role=User.Roles.ADMIN
    )
    user.set_password(password)
    user.save()

    AdminProfile.objects.create(user=user, role=role, phone_number=phone_number)

    return user


def send_password_reset_code(*, email: str) -> bool:
    """
    Sends a password reset code to the given email.
    Returns whether the email has been sent or not.
    """

    try:
        user = User.objects.get(email=email)
        code = PasswordResetCode.objects.create(user=user)
        message = render_to_string(
            "authentication/email/password_reset_code.html", {"code": code}
        )
        send_mail(
            "Password Reset Code",
            message,
            settings.EMAIL_FROM,
            [email],
            html_message=message,
        )
        return True
    except User.DoesNotExist:
        return False


def change_user_password(*, email: str, password: str) -> User:
    """
    Changes the user that owns the given email to the provided password.
    Returns the updated user instance,
    """
    user = User.objects.get(email=email)
    user.set_password(password)
    user.save()

    return User


def update_admin_settings(
    *,
    admin: User,
    first_name: str,
    last_name: str,
    email: str,
    role: str,
    phone_number: str,
    receive_email_notifications: bool,
):
    """
    Updates the given admin account with the provided details.
    """
    admin.first_name = first_name
    admin.last_name = last_name
    admin.email = email
    admin.admin_profile.role = role
    admin.admin_profile.phone_number = phone_number
    admin.admin_profile.receive_email_notifications = receive_email_notifications

    admin.admin_profile.save()
    admin.save()

    return admin
